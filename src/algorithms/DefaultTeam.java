package algorithms;

import java.awt.Point;
import java.util.ArrayList;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class DefaultTeam {
	public ArrayList<Point> calculDominatingSet(ArrayList<Point> points, int edgeThreshold) {
		return gloutonDominatingSet(points, edgeThreshold);
	}

	public ArrayList<Point> gloutonDominatingSet(ArrayList<Point> points, int edgeThreshold) {
		ArrayList<Point> remains = new ArrayList<Point>(points);
		ArrayList<Point> domSet = new ArrayList<Point>();

		while(!isValid(points, domSet, edgeThreshold)){
			Point pMax = remains.get(0);
			for(Point p : remains){
				if(getDegree(p, remains, edgeThreshold) > getDegree(pMax, remains, edgeThreshold)){
					pMax = p;
				}
			}
			remains.remove(pMax);
			domSet.add(pMax);    	
		}

		ArrayList<Point> betterDomSet = localSearch(points, domSet, edgeThreshold);

		while(domSet.size() > betterDomSet.size()){  
			domSet = betterDomSet;
			betterDomSet = localSearch(points, domSet, edgeThreshold);
		}
		System.out.println("DONE");
		return domSet;
	}

	public ArrayList<Point> localSearch(ArrayList<Point> points, ArrayList<Point> domSet , int edgeThreshold) {
		ArrayList<Point> nextDomSet = new ArrayList<Point>(domSet);
		ArrayList<Point> remainingPoints = new ArrayList<Point>(points);
		System.out.println("local search | current size " + domSet.size());
		remainingPoints.removeAll(domSet);
		int distanceThreshold = 3 * edgeThreshold;
		int sqDistanceThreshold = (int) Math.pow(distanceThreshold, 2);
		for(Point p: domSet){
			for(Point q: domSet){
				if(p.equals(q)){ continue; }
				if(p.distanceSq(q) > sqDistanceThreshold){
					continue;
				}
				for(Point z: remainingPoints){
					if(z.distanceSq(p) > sqDistanceThreshold || z.distanceSq(q) > sqDistanceThreshold){
						continue;
					}
					nextDomSet.remove(p);
					nextDomSet.remove(q);
					nextDomSet.add(z);
					if(isValid(points, nextDomSet, edgeThreshold)){
						return nextDomSet;  
					}else{
						nextDomSet.add(p);
						nextDomSet.add(q);
						nextDomSet.remove(z);
					}
				}  
			}
		}

		for(Point p: domSet){
			for(Point q: domSet){
				if(p.equals(q)) { continue; }
				if(p.distanceSq(q) > sqDistanceThreshold){
					continue;
				}
				for(Point r: domSet){
					if(p.equals(r) || q.equals(r)){ continue; }
					if(p.distanceSq(r) > sqDistanceThreshold || q.distanceSq(r) > sqDistanceThreshold){
						continue;
					}
					for(Point x: remainingPoints){
						for(Point y: remainingPoints){
							if(x.equals(y)){ continue; }
							if(x.distanceSq(y) > sqDistanceThreshold){
								continue;
							} 
							nextDomSet.remove(p);
							nextDomSet.remove(q);
							nextDomSet.remove(r);
							nextDomSet.add(x);
							nextDomSet.add(y);
							if(isValid(points, nextDomSet, edgeThreshold)){
								return nextDomSet;  
							}else{
								nextDomSet.add(p);
								nextDomSet.add(q);
								nextDomSet.add(r);
								nextDomSet.remove(x);
								nextDomSet.remove(y);
							}
							}
						}
					}
				}
			}
		return domSet;
	}




		public boolean isValid(ArrayList<Point> points, ArrayList<Point> set, int edgeThreshold){
			ArrayList<Point> remains = new ArrayList<Point>(points);
			remains.removeAll(set); // points that are in the set are ok
			for(Point p: remains){ // testing neighbors
				//		  if(intersection(neighbor(p, points, edgeThreshold), set).isEmpty()){
				//			  return false;
				//		  }
				boolean isConnected = false;
				for(Point q: neighbor(p, points, edgeThreshold)){
					if(set.contains(q)){
						isConnected = true;
						break;
					}
				}
				if(!isConnected){
					return false;
				}
			}
			return true;
		}

		public ArrayList<Point> intersection(ArrayList<Point> list1, ArrayList<Point> list2) {
			ArrayList<Point> list = new ArrayList<Point>();
			for (Point t : list1) {
				if(list2.contains(t)) {
					list.add(t);
				}
			}
			return list;
		}

		public int getDegree(Point p, ArrayList<Point> vertices, int edgeTreshold){
			return neighbor(p, vertices, edgeTreshold).size();
		}

		private ArrayList<Point> neighbor(Point p, ArrayList<Point> vertices, int edgeTreshold){
			ArrayList<Point> result = new ArrayList<Point>();

			for (Point point:vertices) if (point.distance(p)<edgeTreshold && !point.equals(p)) result.add((Point)point.clone());

			return result;
		}


		//FILE PRINTER
		private void saveToFile(String filename,ArrayList<Point> result){
			int index=0;
			try {
				while(true){
					BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(filename+Integer.toString(index)+".points")));
					try {
						input.close();
					} catch (IOException e) {
						System.err.println("I/O exception: unable to close "+filename+Integer.toString(index)+".points");
					}
					index++;
				}
			} catch (FileNotFoundException e) {
				printToFile(filename+Integer.toString(index)+".points",result);
			}
		}
		private void printToFile(String filename,ArrayList<Point> points){
			try {
				PrintStream output = new PrintStream(new FileOutputStream(filename));
				int x,y;
				for (Point p:points) output.println(Integer.toString((int)p.getX())+" "+Integer.toString((int)p.getY()));
				output.close();
			} catch (FileNotFoundException e) {
				System.err.println("I/O exception: unable to create "+filename);
			}
		}

		//FILE LOADER
		private ArrayList<Point> readFromFile(String filename) {
			String line;
			String[] coordinates;
			ArrayList<Point> points=new ArrayList<Point>();
			try {
				BufferedReader input = new BufferedReader(
						new InputStreamReader(new FileInputStream(filename))
						);
				try {
					while ((line=input.readLine())!=null) {
						coordinates=line.split("\\s+");
						points.add(new Point(Integer.parseInt(coordinates[0]),
								Integer.parseInt(coordinates[1])));
					}
				} catch (IOException e) {
					System.err.println("Exception: interrupted I/O.");
				} finally {
					try {
						input.close();
					} catch (IOException e) {
						System.err.println("I/O exception: unable to close "+filename);
					}
				}
			} catch (FileNotFoundException e) {
				System.err.println("Input file not found.");
			}
			return points;
		}
	}
